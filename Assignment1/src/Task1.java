import java.util.Scanner;

public class Task1 {
	public static void main(String[] args) {
        // ----------------- write your code BELOW this line only --------
        // your code here (add lines)
		Scanner myScanner=new Scanner(System.in);
		//Getting input from user:
		int num1=myScanner.nextInt();
		int num2=myScanner.nextInt();
		int num3=myScanner.nextInt();
		//Checking the max value in the following "if" statements
		int max=num1;
		if(num2>max){
			max=num2;
		}
		if(num3>max){
			max=num3;
		}
		//Printing the output - the max value out of the three inputs
		System.out.println(max);
        // ----------------- write your code ABOVE this line only ---------
	}
}
