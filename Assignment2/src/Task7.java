public class Task7 {
	public static void convertInput(int[][] variableNames, String[] students, String[] courses, int[][] studentCourses, int k) {	
		
		// part 1
		for (int i = 0; i < variableNames.length; i++) {
			//Adding a clause to the SAT Solver - only one possible day for the exam
			SATSolver.addClauses(Task5.exactlyOne(variableNames[i]));
		}
		
		// part 2
		boolean[][] conflicts = Task4.findExamConflicts(variableNames.length, studentCourses);
		for (int i = 0; i < conflicts.length; i++) {
			for (int j = i+1; j < conflicts[i].length; j++) {
				if(conflicts[i][j]) { //Checking if there are conflicts
					//Adding a clause to the SAT Solver - making sure the conflicting courses are not at the same day
					SATSolver.addClauses(Task5.notSameDay(variableNames[i], variableNames[j]));
				}
			}
		}
	}
}
