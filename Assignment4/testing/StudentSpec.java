/**
 * Created by shalev on 27/12/2016.
 */
public class StudentSpec {
    public static void main(String[] args) {
        // constructor validations
        contructorValidations();

        Student student = new Student("First", "Last", 123);
        System.out.println("created Student (\"First\", \"Last\", 123)");
        validate(student);
    }

    public static void validate(Student student) {

        Course course = new Course("course", 123, 1);
        Course course2 = new Course("course2", 124, 2);
        // getters
        System.out.println("getFirstName(): " + student.getFirstName());
        System.out.println("getLastName(): " + student.getLastName());
        System.out.println("getId(): " + student.getId());
        System.out.println("getTotalCreditRequired(): " + student.getTotalCreditRequired());

        // registerTo() & isRegisteredTo()
        System.out.println("registerTo(course): " + student.registerTo(course));
        System.out.println("isRegisteredTo(course): " + student.isRegisteredTo(course));
        System.out.println("isRegisteredTo(other course): " + student.isRegisteredTo(course2));
        System.out.println("registerTo(course again): " + student.registerTo(course));

        // addGrade() & setGrade() & calculateAverage()
        try {
            System.out.print("setGrade(new course,90): ");
            student.setGrade(new Course("course", 111, 1), 90);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
        System.out.println("addGrade(course, 90): " + student.addGrade(course, 90));
        System.out.println("calculateAverage(): " + student.calculateAverage());

        System.out.println("addGrade(course again, 100): " + student.addGrade(course, 100));
        System.out.println("calculateAverage(): " + student.calculateAverage());

        System.out.println("setGrade(course,100): " + student.setGrade(course, 100));
        System.out.println("calculateAverage(): " + student.calculateAverage());

        student.registerTo(course2);
        System.out.println("addGrade(course2, 70): " + student.addGrade(course2, 70));
        System.out.println("calculateAverage(): " + student.calculateAverage());

        // computeFinalGrade()
        System.out.println("computeFinalGrade(): " + student.computeFinalGrade());

        MathElectiveCourse course3 = new MathElectiveCourse("course3", 125, 117);
        student.registerTo(course3);
        student.addGrade(course3, 75);
        System.out.println("computeFinalGrade(): " + student.computeFinalGrade());

        MathElectiveCourse course4 = new MathElectiveCourse("course4", 126, 121);
        student.registerTo(course4);
        student.addGrade(course4, 95);

        System.out.println("computeFinalGrade(): " + Math.round(student.computeFinalGrade()));
    }

    public static void contructorValidations() {
        try {
            System.out.print("new Student(\"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz \",\"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz \",1): ");
            new Student("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ", 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Student(\"First2\",\"Last\",1): ");
            new Student("First2", "Last", 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Student(\"First\",\"Last2\",1): ");
            new Student("First", "Last2", 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Student(\"First\",\"Last\",0): ");
            new Student("First", "Last2", 0);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Student(null,\"Last\",1): ");
            new Student(null, "Last", 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Student(\"First\",null,1): ");
            new Student("First", null, 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Student(\"\",\"Last\",1): ");
            new Student("", "Last", 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Student(\"First\",\"\",1): ");
            new Student("First", "", 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
    }
}
