import java.util.Scanner;

public class Task4 {
	public static void main(String[] args) {
        // ----------------- write your code BELOW this line only --------
        // your code here (add lines)
		Scanner myScanner=new Scanner(System.in);
		int m=myScanner.nextInt();//Getting input for the desired value of fib(n)
		int firstNumber=1;
		int secondNumber=1;
		int fib=0;
		//Calculating fibonacci, where the value in less than input
		while(fib<m){
			firstNumber=secondNumber;
			secondNumber=fib;
			fib=firstNumber+secondNumber;
		}
		System.out.println(firstNumber);
		System.out.println(secondNumber);
        // ----------------- write your code ABOVE this line only ---------
	}
}
