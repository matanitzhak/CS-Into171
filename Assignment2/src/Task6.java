public class Task6 {
	public static int[][] variableTable (int m, int k){
		int[][] variableNames = new int[m][k];
		//Setting a different day number for each course
		int dayCounter=0;
		for(int i=0; i<m; i++) {
			for(int j=0; j<k; j++) {
				dayCounter++;
				variableNames[i][j]=dayCounter;
			}
		}

		return variableNames;
	}
}
