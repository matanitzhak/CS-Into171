

import org.junit.Assert;
import org.junit.Test;

public class TestStudentManagement13 {
	
	@Test(timeout=2000)
	public void runTest(){
		StudentManagementSystem sys = new StudentManagementSystem();
		Student s1 = new Student("Alice", "Bob", 1);
		Student s2 = new Student("Bob", "Alice", 10);
		Course c1 = new Course("intro", 101, 5);
		Course c2 = new Course("intro2", 121, 5);

		sys.addCourse(c1);
		sys.addCourse(c2);
		sys.addStudent(s1);
		sys.register(s1, c1);
		sys.register(s1, c2);
		sys.addGradeToStudent(s1, c1, 70);
		
		
		Assert.assertEquals("StudentManagementSystem: addGradeToStudent()", false, sys.addGradeToStudent(s1, c1, 90));
	}
}
