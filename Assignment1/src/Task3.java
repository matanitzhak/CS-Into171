import java.util.Scanner;

public class Task3 {
	public static void main(String[] args) {
        // ----------------- write your code BELOW this line only --------
        // your code here (add lines)
		Scanner myScanner=new Scanner(System.in);
		int n=myScanner.nextInt();//Getting input "n" for the desired fib(n)
		int firstNumber=1;
		int secondNumber=1;
		int fib=0;
		//Calculating fibonacci for the (n+1) number
		for(int i=1; i<=n+1; i++){
			firstNumber=secondNumber;
			secondNumber=fib;
			fib=firstNumber+secondNumber;
		}
		System.out.println(fib);//fib(n+1)
		System.out.println(fib+secondNumber);//fib(n+2)
        // ----------------- write your code ABOVE this line only ---------
	}
}
