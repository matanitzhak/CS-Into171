

import org.junit.Assert;
import org.junit.Test;

public class TestComputeFinalGrade06 {
	
	@Test(timeout=2000)
	public void runTest(){
		StudentManagementSystem sys = new StudentManagementSystem();
		Student s = new Student("Alice", "Bob", 777);
		Course c1 = new CsElectiveCourse("intro", 202, 50);
		Course c2 = new MathElectiveCourse("math", 1, 10);
		Course c3 = new Course("Course", 5, 60);
		sys.addCourse(c1);
		sys.addCourse(c2);
		sys.addCourse(c3);
		sys.addStudent(s);
		sys.register(s, c1);
		sys.register(s, c2);
		sys.register(s, c3);
		sys.addGradeToStudent(s, c1, 60);
		sys.addGradeToStudent(s, c2, 70);
		
		Assert.assertEquals("Student: computeFinalGrade()", -1, s.computeFinalGrade(), 0.0);
	}
}
