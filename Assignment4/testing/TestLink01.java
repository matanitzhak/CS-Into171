


import org.junit.Assert;
import org.junit.Test;

public class TestLink01 {
	
	@Test(timeout=2000)
	public void runTest(){
		Link l1 = new Link(new Integer(5));
		Link l2 = new Link(new Integer(5));
		
		Assert.assertEquals("Link: setData()", true, l1.equals(l2));
	}
}
