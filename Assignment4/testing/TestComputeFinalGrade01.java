import org.junit.Assert;
import org.junit.Test;

public class TestComputeFinalGrade01 {
	
	@Test(timeout=2000)
	public void runTest(){
		Course c = new Course("intro", 202, 5);
		Grade g = new Grade(c, 70);
		
		Assert.assertEquals("Grade: computeFinalGrade()", 70, g.computeFinalGrade());
	}
}
