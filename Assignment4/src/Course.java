
public class Course {
	
	private String name;
	private int number;
	private int credit;

	public Course(String name, int number, int credit){
		if(!isNameValid(name) || credit<=0 || number<=0) { //given parameters are not legal
			throw new IllegalArgumentException("Input is not valid");
		}
		this.name=name;
		this.number=number;
		this.credit=credit;
	}

	public String getCourseName(){
		return this.name;
	}

	public int getCourseNumber(){
		return this.number;
	}

	public int getCourseCredit(){
		return this.credit;
	}

	public String toString(){
		String description=this.name+", id:"+this.number+", "+this.credit+" points";
		return description;
	}

	public boolean equals(Object other){
		if(!(other instanceof Course)) { //if object is not an instance of Course, it is not equals
			return false;
		}
		Course otherCourse=(Course)other; //casting the object to 'Course'
		if(this.getCourseNumber()==otherCourse.getCourseNumber()) { //if it has the same course number, they are the same
			return true;
		}
		else {
			return false;
		}
	}

	public int computeFinalGrade(int grade){
		if(grade<0 || grade>100) { //wrong input - must be between 0 to 100
            throw new IllegalArgumentException("Grade must be on the scale of 0 to 100");
        }
		//this is regular course - return the grade as it is
        return grade;
	}
	
	private boolean isNameValid(String str) {
		if(str==null || str.length()==0) { //checking if the string is null or has a length of 0 - in that case, it's not valid
			return false;
		}
		str=str.toLowerCase();
		for(int i=0; i<str.length(); i++) {
			//Checks if the string has only English letters \ numbers \ space
			if((str.charAt(i)>='a' && str.charAt(i)<='z') || (str.charAt(i)>='0' && str.charAt(i)<='9') || str.charAt(i)==' ') {
			
			}
			else {
				return false;
			}
		}
		return true;
	}
}
