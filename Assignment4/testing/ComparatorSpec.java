/**
 * Created by shalev on 27/12/2016.
 */
public class ComparatorSpec {
    public static void validateCompare(Comparator comp, Object obj1, Object obj2) {
        System.out.println("compare(obj1,obj2): " + (int) (Math.signum(comp.compare(obj1, obj2)))); // negative to -1, positive to 1
        System.out.println("compare(obj2,obj1): " + (int) (Math.signum(comp.compare(obj2, obj1))));
        System.out.println("compare(obj1,obj1): " + comp.compare(obj1, obj1));
    }
}
