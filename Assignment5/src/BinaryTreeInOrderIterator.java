import java.util.Iterator;
import java.util.NoSuchElementException;

public class BinaryTreeInOrderIterator implements Iterator{
	private Stack stack;
 
	public BinaryTreeInOrderIterator(BinaryNode root) {
		this.stack=new StackAsDynamicArray(); //initializing the stack - as dynamic array
		BinaryNode node=root;
		 //first node to be visited will be the left one
		 while (node != null) {
            stack.push(node);
            node = node.left;
        }
	}
 
	public boolean hasNext() {
		return !stack.isEmpty(); //if empty - there isn't a next element and vice versa
	}
 
	public Object next() {
		if (stack.isEmpty()) { //stack is empty - no next object available 
			throw new NoSuchElementException("stack is empty");
		}
		BinaryNode node=(BinaryNode) stack.pop(); //getting the next node
		Object data=node.data; //saving it's data
		if (node.right != null) {
            node = node.right; //getting the right subtree
             
            // the next node to be visited is the leftmost
            while (node != null) {
                stack.push(node);
                node = node.left;
            }
        }
	    return data;
	}
	
	@Override
	public void remove(){
		throw new UnsupportedOperationException();
	}
}
