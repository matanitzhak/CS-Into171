/**
 * Created by shalev on 27/12/2016.
 */
public class LinkedListSpec {
    public static void main(String[] args) {
        LinkedList linkedList = new LinkedList();

        // isEmpty()
        System.out.println("isEmpty(): " + linkedList.isEmpty());
        linkedList.add("data");
        System.out.println("isEmpty(): " + linkedList.isEmpty());

        // size()
        System.out.println("size(): " + linkedList.size());
        linkedList.add("data2");
        System.out.println("size(): " + linkedList.size());

        // contains
        System.out.println("contains(data): " + linkedList.contains("data"));
        System.out.println("contains(nodata): " + linkedList.contains("nodata"));

        // get()
        System.out.println("get(0): " + linkedList.get(0));
        System.out.println("get(1): " + linkedList.get(1));
        try {
            System.out.print("get(-1): ");
            linkedList.get(-1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
        try {
            System.out.print("get(2): ");
            linkedList.get(2);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        // set()
        System.out.println("set(1, newdata): " + linkedList.set(1, "newdata"));
        System.out.println("get(1): " + linkedList.get(1));
        linkedList.set(1, "data2");
        try {
            System.out.print("set(-1, \"data\"): ");
            linkedList.set(-1, "data");
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
        try {
            System.out.print("set(2, \"data\"): ");
            linkedList.set(2, "data");
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
        try {
            System.out.print("set(1, null): ");
            linkedList.set(1, null);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        // equals()
        LinkedList linkedList2 = new LinkedList();
        System.out.println("equals(self): " + linkedList.equals(linkedList));
        System.out.println("equals(empty linkedList): " + linkedList.equals(linkedList2));
        linkedList2.add("data");
        System.out.println("equals(linkedList with less data): " + linkedList.equals(linkedList2));
        linkedList2.add("diff_data");
        System.out.println("equals(linkedList different data): " + linkedList.equals(linkedList2));
        linkedList2.set(1, "data2");
        System.out.println("equals(linkedList same data): " + linkedList.equals(linkedList2));
        linkedList2.add("data3");
        System.out.println("equals(linkedList more data): " + linkedList.equals(linkedList2));

        // sortBy()
        LinkedList linkedList3 = new LinkedList();
        linkedList3.add("bbb");
        linkedList3.add("aaa");
        linkedList3.add("ccc");
        linkedList3.sortBy(new StringComparator());
        System.out.println("sortBy(), after sort: + " + linkedList3.get(0));
        System.out.println("sortBy(), after sort: + " + linkedList3.get(1));
        System.out.println("sortBy(), after sort: + " + linkedList3.get(2));
        try {
            System.out.print("sortBy(null): ");
            linkedList.sortBy(null);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
    }
}

class StringComparator implements Comparator {
    @Override
    public int compare(Object obj1, Object obj2) {
        String str1 = (String) obj1;
        String str2 = (String) obj2;
        return str1.compareTo(str2);

    }
}