

import org.junit.Assert;
import org.junit.Test;

public class TestStudent02 {
	
	@Test(timeout=2000)
	public void runTest(){
		Student s = new Student("Alice" , "Bob", 1);
		
		Assert.assertEquals("Student: getFirstName()", "Alice", s.getFirstName());
	}
}
