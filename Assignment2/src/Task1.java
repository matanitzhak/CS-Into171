public class Task1 {
	public static void printStudentData(String[] students, String[] courses, int[][] studentCourses, int k) {
		
		if(!Task0.legalData(students, courses, studentCourses, k)) {
			throw new IllegalArgumentException("The input does not follow needed criterias");
		}
		for(int i=0; i<studentCourses.length; i++) {
			//Printing the student's name:
			System.out.print(students[i]+": ");
			for(int j=0; j<studentCourses[i].length; j++) { //going over the student's courses
				int courseID=studentCourses[i][j];//getting the course ID
				//Printing the course's name:
				if(j+1<studentCourses[i].length) {
					System.out.print(courses[courseID]+", ");
				}
				else {
					System.out.println(courses[courseID]);
				}
			}
		}
	
	}
}
