// You may not change or erase any of the lines and comments 
// in this file. You may only add lines.

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args){
        // ----------------- write any code BELOW this line only --------
        // your code here (add lines)
    	boolean isVerified=true;
    	int x1,x2,x3,x4;
    	//Going over all possible combinations of 0 and 1 in 4-digits number
    	//Each for-loop is responsible for a certain digit
    	//The nested loops allow us to go over all possible combinations
    	for(int i=0; i<2 && isVerified; i++) {
    		for(int j=0; j<2 && isVerified; j++) {
    			for(int y=0; y<2 && isVerified; y++) {
    				for(int z=0; z<2 && isVerified; z++) {
    					x1=i;
    					x2=j;
    					x3=y;
    					x4=z;
            // ----------------- write any code ABOVE this line only ---------




            // -----------------  copy here the code from Task 9 that is between
            // -----------------  the comments "A" and "B"
            // code from Task 9 here
    					int temp;
    		            //Putting the smallest number among (x1,x2) in x1
    		            if(x1>x2) {
    		    			temp=x2;
    		    			x2=x1;
    		    			x1=temp;
    		    		}
    		            //Putting the smallest number among (x3,x4) in x3
    		            if(x3>x4) {
    		    			temp=x4;
    		    			x4=x3;
    		    			x3=temp;
    		    		}
    		            //Right now we have: [minValue of first two, maxValue of first two, minValue of last two, maxValue of last two]
    		            //Putting the smallest number among (x1,x3) in x1 - comparing the "minValue"
    		    		if(x1>x3) {
    		    			temp=x3;
    		    			x3=x1;
    		    			x1=temp;
    		    		}
    		    		//Right now we have: [minValue of all, maxValue of last two, maxValue of minValue, maxValue of last two]
    		            //Putting the smallest number among (x2,x4) in x2 - comparing the "maxValue" of the biggest (first&last two)
    		    		if(x2>x4) {
    		    			temp=x4;
    		    			x4=x2;
    		    			x2=temp;
    		    		}
    		    		//Right now we have: [minValue of all, minValue of maxValue, maxValue of minValue, maxValue of all]
    		            //Putting the smallest number among (x2,x3) in x2 - comparing the middle parts
    		    		if(x2>x3) {
    		    			temp=x3;
    		    			x3=x2;
    		    			x2=temp;
    		    		}
    		    		//Now we got the numbers sorted 
        // -----------------  end of copied code from Task 9




        // ----------------- write any code BELOW this line only --------
        // your code here (add lines)
    		    		//Checking if the numbers are sorted
						if(x1<=x2 && x2<=x3 && x3<=x4) {
							isVerified=true;
						}
						else {
							isVerified=false;
							System.out.println(x1);
							System.out.println(x2);
				            System.out.println(x3);
				            System.out.println(x4);
						}
    				}
    			}
    		}
    	}
    	if(isVerified) {
    		System.out.println("verified");
    	}
        // ----------------- write any code ABOVE this line only ---------

    } // end of main
} //end of class Task10

