import java.util.Iterator;

public class Student {
	private String firstName;
	private String lastName;
	private int id;
	private LinkedList courses;
	private LinkedList grades;
	private static final int REQUIRED_CREDIT=120;

	public Student(String firstName, String lastName, int id) {
		if(!isNameValid(firstName) || !isNameValid(lastName) || id<=0) {
			throw new IllegalArgumentException("Input is not valid");
		}
		this.firstName=firstName;
		this.lastName=lastName;
		this.id=id;
		this.courses=new LinkedList();
		this.grades=new LinkedList();
	}


	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public int getId() {
		return this.id;
	}

	public boolean isRegisteredTo(Course course){
		return this.courses.contains(course); //see if the course appears in the courses' list 
        /*
		Iterator myIterator=this.courses.iterator();
		while(myIterator.hasNext()) {
			Course myCourse=(Course)myIterator.next();
			if(myCourse.equals(course)) {
				return true;
			}
		}
		return false;
		*/
	}

	public boolean registerTo(Course course){
		if(this.isRegisteredTo(course)) { //the student is already registered to that course, action cannot be done
			return false;
		}
		this.courses.add(course); //add that course to the list of courses the student takes
		return true;
	}

	public double calculateAverage(){
		double totalCredit=0;
		double totalWeightedSum=0;
		Iterator gradeIterator=this.grades.iterator();
		while(gradeIterator.hasNext()) { //going over the list of grades with iterator
			Grade myGrade=(Grade)gradeIterator.next();
			int courseCredit=myGrade.getCourse().getCourseCredit();
			totalCredit+=courseCredit; //adding the courese's credit to the total amount of credit
			totalWeightedSum+=myGrade.getGrade()*courseCredit; //adding the grade*credit to the total weighted sum of grades
		}
		if(totalCredit>0) { //return the average
			return totalWeightedSum/totalCredit;
		}
		else{ //no grades - return 0
			return 0;
		}
	}

	public boolean addGrade(Course course, int grade){
		if(!this.isRegisteredTo(course)||this.hasGradeToCourse(course)){
            //the student is not registered to that course or already has grade to that course, action cannot be done
			return false;
		}
		Grade newGrage=new Grade(course, grade); //creating the new grade
		this.grades.add(newGrage); //adding that grade to the list of grades
		return true;
	}
	
	public int setGrade(Course course, int grade){
		if(!this.isRegisteredTo(course)||!this.hasGradeToCourse(course)) {
            //the student is not registered to that course or doesn't have grade to that course, action cannot be done
			throw new IllegalArgumentException("Input is not valid - Student has no grade in that course");
		}
		Iterator gradeIterator=this.grades.iterator();
		while(gradeIterator.hasNext()) { //going over the list of grades with iterator
			Grade myGrade=(Grade)gradeIterator.next();
			if(myGrade.getCourse().equals(course)) { //we've reached the desired course - set a new grade and return the old one
				return myGrade.setGrade(grade);
			}
		}
		return -1; //couldn't find the desired grade, return -1
	}

	public String toString(){
		String description="Student - "+this.firstName+" "+this.lastName+" (id:"+this.getId()+");";
		description+="\tCourses: "+this.courses.toString()+";";
		description+="\t Grades: "+this.grades.toString();
		return description;
	}

	public boolean equals(Object other){
		if(!(other instanceof Student)) { //object is not an instance of Student, meaning it's not equal
			return false;
		}
		Student otherStudent=(Student)other;
		if(this.getId()!=otherStudent.getId()) { //the students don't have the same id, meaning they are not the same
			return false;
		}
		return true;
	}

	public int getTotalCreditRequired(){
		return REQUIRED_CREDIT;
	}

	public double computeFinalGrade(){
		FinalGradeComparator finalGradeComparator=new FinalGradeComparator(); //creating a new comparator
        //soring the grades' list according to the comparator, so the highest grades will be placed first
		this.grades.sortBy(finalGradeComparator); 
		double totalCredit=0;
		double totalWeightedSum=0;
		double avg;
		Iterator gradeIterator=this.grades.iterator();
        //going over the list with iterator, until we get to the total credit required
		while(gradeIterator.hasNext() && totalCredit<this.getTotalCreditRequired()) {
			Grade myGrade=(Grade)gradeIterator.next();
			int finalGrade=myGrade.computeFinalGrade();
			if(finalGrade>=56) { //include the grade only if student have passed the course
				int courseCredit=myGrade.getCourse().getCourseCredit();
				totalCredit+=courseCredit; //adding the courese's credit to the total amount of credit
				totalWeightedSum+=finalGrade*courseCredit; //adding the grade*credit to the total weighted sum of grades
			}
		}
		if(totalCredit>=this.getTotalCreditRequired()) { //if we've reached the total credit required - return the average
			return totalWeightedSum/totalCredit;
		}
		else { //no grades - return -1
			return -1;
		}
	}
	
	private boolean isNameValid(String str) {
		if(str==null || str.length()==0) { //checking if the string is null or has a length of 0 - in that case, it's not valid
			return false;
		}
		str=str.toLowerCase();
		for(int i=0; i<str.length(); i++) {
			//Checks if the string has only English letters \ space
			if((str.charAt(i)>='a' && str.charAt(i)<='z') || str.charAt(i)==' ') {
			
			}
			else {
				return false;
			}
		}
		return true;
	}
	
	private boolean hasGradeToCourse(Course course) {
		Iterator gradeIterator=this.grades.iterator();
		while(gradeIterator.hasNext()) { //going over the grades' list with iterator
			Grade myGrade=(Grade)gradeIterator.next();
			if(myGrade.getCourse().equals(course)) { //we got the same course - return true
				return true;
			}
		}
		return false;
	}
}
