import java.util.Scanner;

public class Task6 {
	public static void main(String[] args) {
        // ----------------- write your code BELOW this line only --------
        // your code here (add lines)
		Scanner myScanner=new Scanner(System.in);
		int input=myScanner.nextInt();
		int m=2;
		int n=1;
		int temp;
		//We can see that the input should be the reminder
		//In addition, m and n parameters are acting like Fibonacci Sequence in that case
		//Meaning that r=n, m=n and m=n+r 
		//So, in order to get the desired numbers we'll run through the numbers in Fibonacci Sequence "input" times
		for(int i=0; i < input; i++) {
			temp=m;
			m+=n;
			n=temp;
		}
		System.out.println(n);
		System.out.println(m);
        // ----------------- write your code ABOVE this line only ---------
	}
}
