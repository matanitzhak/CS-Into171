

import org.junit.Assert;
import org.junit.Test;

public class TestStudentManagement15 {
	
	@Test(timeout=2000)
	public void runTest(){
		StudentManagementSystem sys = new StudentManagementSystem();
		int n = 13;
		for (int i=0; i < n; i++) {
			Student s = new Student("first", "last", i+1);
			sys.addStudent(s);
		}
		
		Assert.assertEquals("StudentManagementSystem:  getNumberOfStudents()", n, sys.getNumberOfStudents());
	}
}
