

import org.junit.Assert;
import org.junit.Test;

public class TestLinkedList11 {
	
	@Test(timeout=2000)
	public void runTest(){
		LinkedList lst = new LinkedList();
		for (int i=0; i < 5; i++)
		{
			lst.add(new Integer(i));
		}
	
		Assert.assertEquals("LinkedList: contains()", false, lst.contains(new Integer(12)));
	}
}
