public class Task0 {
	public static boolean legalData(String[] students, String[] courses, int[][] studentCourses, int k) {
		//Validating 'k' value:
		if(k<=0) { //wrong number of days
			return false;
		}
		
		//Validating 'courses' and 'students' arrays:
		if(!validatingExistence(courses)) {
			return false;
		}
		if(!validatingExistence(students)) {
			return false;
		}
		
		//Validating students names:
		if(!validatingNames(students)) {
			return false;
		}
		
		//Validating courses names:
		if(!validatingNames(courses)) {
			return false;
		}
		
		//Validating 'studentCourses' length according to 'students' length:
		if(studentCourses.length!=students.length) { //different length of students in 'students' and 'studentCourses' arrays
			return false;
		}
		
		//Validating 'studentCourses' array and its content:
		if(studentCourses==null || studentCourses.length==0 || studentCourses[0].length==0) {
			return false;
		}
		for(int i=0; i<studentCourses.length; i++) {
			for(int j=0; j<studentCourses[i].length; j++) {				
				if(studentCourses[i][j]<0) { // wrong course id (or "null" object)
					//TODO: ask about "does not contain null", because int can't be null by definition
					return false;
				}
				for(int x=j+1; x<studentCourses[i].length; x++) {
					if(studentCourses[i][j]==studentCourses[i][x]) { //has course id duplicates
						return false;
					}
				}
			}
		}
		
		//Checking that each student has at least one course and vise versa:
		if(!atLeastOneAppearance(courses.length, studentCourses)) {
			return false;
		}
		
		//Everything is fine - return true
		return true;
	}
	
	public static boolean validatingExistence(String[] arr) {
		if(arr==null || arr.length==0) { //null or empty array
			return false;
		}
		return true;
	}
	
	public static boolean validatingNames(String[] arr) {
		for(int i=0; i<arr.length; i++) {
			if(arr[i]==null || arr[i].equals("")) { //does not contain name
				return false;
			}
			for(int j=i+1; j<arr.length; j++) {
				if(arr[i].equals(arr[j])) { //has name duplicates
					return false;
				}
			}
		}
		return true;
	}
	
	public static boolean atLeastOneAppearance (int numberOfCourses, int[][] studentCourseScheduling) {
		int[] coursesScheduling=new int[numberOfCourses];
		//Initializing coursesScheduling array:
		for(int i=0; i<coursesScheduling.length; i++) {
			coursesScheduling[i]=0;
		}
		//Checking that each student has at least one course:
		for(int courseIndex=0; courseIndex<coursesScheduling.length; courseIndex++) {
			for(int i=0; i<studentCourseScheduling.length; i++) {
				if(studentCourseScheduling[i].length<1) { //no courses for this student
					return false;
				}
				for(int j=0; j<studentCourseScheduling[i].length; j++) {
					if(studentCourseScheduling[i][j]==courseIndex) {
						coursesScheduling[courseIndex]++; //another student is taking this course
					}
				}
			}
		}
		//Checking that each course has at least one student:
		for(int i=0; i<coursesScheduling.length; i++) {
			if(coursesScheduling[i]<1) { //no students for this course
				return false;
			}
		}
		return true;
	}
}
