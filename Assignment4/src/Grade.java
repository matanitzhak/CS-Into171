
public class Grade {
	private Course course;
	private int grade;
	
	public Grade(Course course, int grade){
		if(course==null || !isGradeValid(grade)) { //if course is null or grade is not valid - the input is not OK
			throw new IllegalArgumentException("Input is not valid");
		}
		this.course=course;
		this.grade=grade;
	}

	public int getGrade() {
		return this.grade;
	}

	public int setGrade(int grade) {
		if(!isGradeValid(grade)) {
			throw new IllegalArgumentException("Grade must be between 0 to 100");
		}
		int oldGrade=this.grade;
		this.grade=grade; //setting the new grade
		return oldGrade; //returning the old grade
	}

	public Course getCourse() {
		return this.course;
	}
	
	public String toString(){
		String description=this.course.getCourseName()+": "+this.grade;
		return description;
	}
	
	public boolean equals(Object other){
		if(!(other instanceof Grade)) { //object is not an instance of Grade, meaning it's not equal
			return false;
		}
		Grade otherGrade=(Grade)other;
        //if the other object has the same course and the same grade - they are the same
		if(this.getCourse().equals(otherGrade.getCourse()) && this.getGrade()==otherGrade.getGrade()) {
			return true;
		}
		else{
			return false;
		}
	}
	
	public int computeFinalGrade(){
		return this.course.computeFinalGrade(this.grade);
	}
	
	private boolean isGradeValid(int grade) {
        //legal grade is between 0 to 100
		if(grade<0 || grade>100) {
			return false;
		}
		return true;
	}
}
