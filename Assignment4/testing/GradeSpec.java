/**
 * Created by shalev on 27/12/2016.
 */
public class GradeSpec {
    public static void main(String[] args) {
        // constructor validations
        contructorValidations();

        Course course = new Course("course", 123, 1);
        Grade grade = new Grade(course, 70);
        System.out.println("created Grade(\"course\", 70)");

        // getters
        System.out.println("getGrade(): " + grade.getGrade());
        System.out.println("getCourse() is course: " + (grade.getCourse() == course)); // check with == for same object

        // setGrade()
        try {
            System.out.print("setGrade(-1): ");
            grade.setGrade(-1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
        try {
            System.out.print("setGrade(101): ");
            grade.setGrade(101);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
        System.out.println("setGrade(80): " + grade.setGrade(80));
        System.out.println("getGrade(): " + grade.getGrade());

        // equals()
        System.out.println("equals(self): " + grade.equals(grade));

        Grade grade2 = new Grade(course, 80);
        System.out.println("equals(same course and grade): " + grade.equals(grade2));

        Grade grade3 = new Grade(new Course("course2", 124, 1), 80);
        System.out.println("equals(different course): " + grade.equals(grade3));

        Grade grade4 = new Grade(course, 70);
        System.out.println("equals(different grade): " + grade.equals(grade4));
    }

    public static void contructorValidations() {
        try {
            System.out.print("new Grade(\"course\",50): ");
            new Grade(new Course("course", 123, 1), 50);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Grade(null,50): ");
            new Grade(null, 50);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Grade(\"course\",-1): ");
            new Grade(new Course("course", 123, 1), -1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Grade(\"course\",101): ");
            new Grade(new Course("course", 123, 1), 101);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
    }
}