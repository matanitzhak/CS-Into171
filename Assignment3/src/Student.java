public class Student {
	
	private static int MAX_NUMBER_OF_COURSES=10;
	private String name;
	private int id;
	private Course[] myCoursesArray;
	private int myCoursesNumber;

	public Student(String name,int id){
		if(!isNameValid(name) || id<1) {
			throw new IllegalArgumentException("Input is not valid");
		}
		this.name=name;
		this.id=id;
		this.myCoursesArray=new Course[MAX_NUMBER_OF_COURSES];
		this.myCoursesNumber=0;
	}

	public String getName(){
		return this.name;
	}

	public int getID(){
		return this.id;
	}

	private Course[] getMyCoursesArray() {
		return myCoursesArray;
	}

	private int getMyCoursesNumber() {
		return myCoursesNumber;
	}

	public boolean registerTo(Course course){
		//If the student is already registered to the course or has taken the max number of courses, you cannot register this course
		if(this.myCoursesNumber>=MAX_NUMBER_OF_COURSES || this.isRegisteredTo(course)) {
			return false;
		}
		//Else - adding the course to the array and increasing the number of courser for that student
		this.myCoursesArray[myCoursesNumber]=course;
		myCoursesNumber++;
		return true;
	}

	public boolean isRegisteredTo(Course course){
		for(int i=0; i<this.myCoursesNumber; i++) { //going over the student's courses and checking if it equals the given course
			Course myCourse=this.myCoursesArray[i];
			if(myCourse.isEqualTo(course)) {
				return true;
			}
		}
		return false;
	}

	public String toString(){
		//Printing information about the student
		String description=this.name+", id-"+this.id;
		description+="\nRegistered to "+this.myCoursesNumber+" courses:";
		//Printing the student's courses
		for(int i=0; i<this.myCoursesNumber; i++) {
			Course myCourse=this.myCoursesArray[i];
			description+="\n"+myCourse.toString();
		}
		return description;
	}
	
	public boolean isEqualTo(Student other){
		return this.id==other.getID();
	}
	
	private boolean isNameValid(String str) {
		if(str==null || str.length()==0) {
			return false;
		}
		str=str.toLowerCase();
		for(int i=0; i<str.length(); i++) {
			//Checks if the string has only English letters \ space
			if((str.charAt(i)>='a' && str.charAt(i)<='z') || str.charAt(i)==' ') {
				
			}
			else {
				return false;
			}
		}
		return true;
	}
}
