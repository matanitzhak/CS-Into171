public class Task5 {
	public static int[][] atLeastOne(int[] vars)
	{
		int numOfClause = 1;
		int numOfLiteralsInClause = vars.length;
		int[][] formula = new int[numOfClause][numOfLiteralsInClause];
		//Making sure that the minimum is one true, no matter the literals' input
		//We do it by setting the same values of the vars to the literals
		for(int i=0; i<formula.length; i++) {
			for(int j=0; j<vars.length; j++) {	
				formula[i][j]=vars[j];
			}
		}
		
		return formula;
	}
	public static int[][] atMostOne(int[] vars)
	{
		int n = vars.length;
		int numOfVarsPairs = n * (n-1) / 2;
		int numOfLiteralsInClause = 2;
		int[][] formula = new int[numOfVarsPairs][numOfLiteralsInClause];
		//Making sure that the maximum is one true, no matter the literals' input
		//We do it by setting the negative values of the vars to the literals
		int row=0;
		int times=0;
		int i=0;
		int j=1;
		while(row<numOfVarsPairs) {
			while(times<vars.length-i-1) {
				formula[row][0]=-vars[i];
				formula[row][1]=-vars[j];
				row++;
				times++;
				j++;
			}
			i++;
			j=i+1;
			times=0;
		}
		
		return formula;
	}
	public static int[][] exactlyOne(int[] vars)
	{
		int n = vars.length;
		int numOfVarsPairs = n * (n-1) / 2;
		int numOfClauses = numOfVarsPairs + 1;
		int[][] formula = new int[numOfClauses][];
		//Making sure that there is *only* one true, no matter the literals' input
		//We do it by combining the "atLeastOne" and "atMostOne" methods
		int[][] atLeastArr=atLeastOne(vars);
		int[][] atMostArr=atMostOne(vars);
		int row=0;
		for(int i=0; i<atLeastArr.length; i++) {
			formula[row]=new int[atLeastArr[i].length];
			for(int j=0; j<atLeastArr[i].length; j++) {
				formula[row][j]=atLeastArr[i][j];
			}
			row++;
		}
		for(int i=0; i<atMostArr.length; i++) {
			formula[row]=new int[atMostArr[i].length];
			for(int j=0; j<atMostArr[i].length; j++) {
				formula[row][j]=atMostArr[i][j];
			}
			row++;
		}
		
		
		return formula;
	}
	public static int[][] notSameDay(int[] vars1, int[] vars2) {
		int numOfClause = vars1.length;
		int numOfLiteralsInClause = 2;
		int[][] formula = new int[numOfClause][numOfLiteralsInClause];
		
		int[] literalsPair=new int[2];
		for(int i=0; i<formula.length; i++) {
			//Setting a pair of literals - one from vars1 and one from vars2
			literalsPair[0]=vars1[i];
			literalsPair[1]=vars2[i];
			//Sending that literals to the "atMostOne" method
			int[][] result=atMostOne(literalsPair);
			//Adding the result to the formula
			formula[i][0]=result[0][0];
			formula[i][1]=result[0][1];
		}
		
		return formula;
	}
}
