/**
 * Created by shalev on 27/12/2016.
 */
public class ElectiveCourseSpec {
    public static void main(String[] args) {
        ElectiveCourse course = new ElectiveCourse("course", 123, 1);
        System.out.println("created ElectiveCourse (\"course\", 123, 30)");
        CourseSpec.validate(course);
    }
}
