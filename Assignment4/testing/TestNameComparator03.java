

import org.junit.Assert;
import org.junit.Test;

public class TestNameComparator03 {
	
	@Test(timeout=2000)
	public void runTest(){
		Comparator nameComp = new StudentNameComparator();
		Student s1 = new Student("Bob", "Alice", 1);
		Student s2 = new Student("Alice", "Alice", 10);
		
		
		Assert.assertEquals("StudentNameComparator: compare()", true, (nameComp.compare(s1, s2) > 0));	
	}
}
