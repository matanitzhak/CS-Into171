

import org.junit.Assert;
import org.junit.Test;

public class TestComputeFinalGrade02 {
	
	@Test(timeout=2000)
	public void runTest(){
		Course c = new ElectiveCourse("intro", 202, 5);
		Grade g = new Grade(c, 70);
		
		Assert.assertEquals("Grade: computeFinalGrade()", 77, g.computeFinalGrade());
	}
}
