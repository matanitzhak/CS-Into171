

import org.junit.Assert;
import org.junit.Test;

public class TestStudent14 {
	
	@Test(timeout=2000)
	public void runTest(){
		Student s = new Student("Alice" , "Bob", 1);
		Course c = new Course("intro", 202, 80);
		Course c2 = new Course("course", 222, 80);
		s.registerTo(c);
		s.registerTo(c2);
		s.addGrade(c, 90);
		s.addGrade(c2, 70);
		
		Assert.assertEquals("Student: calculateAverage()", 80, s.calculateAverage(), 0.01);
	}
}
