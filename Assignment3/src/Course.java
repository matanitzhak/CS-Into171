public class Course {

	private String name;
	private int number;

	public Course(String name, int number) {
		if(!isNameValid(name) || number<1) {
			throw new IllegalArgumentException("Input is not valid");
		}
		this.name=name;
		this.number=number;
	}

	public String getName(){
		return this.name;
	}

	public int getCourseNumber(){
		return this.number;
	}

	public String toString(){
		return this.name+"\t(Course number: "+this.number+")";
	}

	public boolean isEqualTo(Course other){
		return this.number==other.getCourseNumber();
	}

	private boolean isNameValid(String str) {
		if(str==null || str.length()==0) {
			return false;
		}
		str=str.toLowerCase();
		for(int i=0; i<str.length(); i++) {
			//Checks if the string has only English letters \ numbers \ space
			if((str.charAt(i)>='a' && str.charAt(i)<='z') || (str.charAt(i)>='0' && str.charAt(i)<='9') || str.charAt(i)==' ') {
			
			}
			else {
				return false;
			}
		}
		return true;
	}
}
