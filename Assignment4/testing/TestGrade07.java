

import org.junit.Assert;
import org.junit.Test;

public class TestGrade07 {
	
	@Test(timeout=2000)
	public void runTest(){

		boolean ans = false;
		Course c = new Course("intro", 202, 5);
		Grade g = new Grade(c ,80);
		try {
			g.setGrade(200);
		}
		catch (IllegalArgumentException e) {
			ans = true;
		}
		
		if(!ans)
			Assert.fail("Grade: Expected IllegalArgumentException");
		
	}
}
