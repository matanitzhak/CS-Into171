public class Task4 {
	public static boolean[][] findExamConflicts(int m, int[][] studentCourses) {
		boolean[][] table = new boolean[m][m];

		for(int i=0; i<studentCourses.length; i++) {//Going over each student
			int[] myCourses=studentCourses[i];//Focusing on the stundet's courses (when we take the sub-array as a one dimensional array
			for(int j=0; j<myCourses.length; j++) {
				for(int x=j+1; x<myCourses.length; x++) {
					int firstCourseIndex=myCourses[j];//Saving first course index
					int secondCourseIndex=myCourses[x];//Saving second course index
					//Marking the conflicts in the boolean array
					table[firstCourseIndex][secondCourseIndex]=true;
					table[secondCourseIndex][firstCourseIndex]=true;
				}
			}
		}
		
		return table;
	}
}
